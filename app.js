
// Create a worker and assign a worker to a bank
const work = new Work()
const bank = new Bank(work) 
let loanGranted = false
// Create a computer array
const computerArray = []
const computer1 = new Computer("Asus",6300, "This is a very good computer", ["Fast", " Light",  " Good screen"], "https://www.notebookcheck.net/uploads/tx_nbc2/ASUS_D509DA-BR128_02.jpg", "https://logos-world.net/wp-content/uploads/2020/07/Asus-Logo.png")
const computer2 = new Computer("Dell", 5400, "This is a  good computer", ["Decent in speed"," Cheep"," Gets hot"], "https://assets.pcmag.com/media/images/581438-meet-the-dell-xps-13-7390.jpg", "https://1000logos.net/wp-content/uploads/2017/07/dell-symbol.jpg")
const computer3 = new Computer("Alienware", 10000, "You need to work alot in order to get this one", ["Fastest"," Expensive"," Awesome"], "https://i.pcmag.com/imagery/reviews/05NFRGrJcf8iz4j4nbdeu9G-4..v_1569474147.jpg", "https://ih1.redbubble.net/image.937806596.9935/flat,750x,075,f-pad,750x1000,f8f8f8.jpg")
computerArray.push(computer1)
computerArray.push(computer2)
computerArray.push(computer3)

// to make sure that the computers is readto the page 
readComputer()
function readComputer() {
    let compSelector = document.getElementById("Selection")
    computerArray.forEach(computer => {
        const option = document.createElement("option")
        option.innerText = computer.name 
        compSelector.appendChild(option)
    })
}

function workButton() {
    work.addMoney()
}

function getMoneyFromWork() {
    work.addMoneyToBank()
}
// Trys to take a loan 
// if the user already has taken a loan the 
function takeLoan() {
    if(bank.account === 0) {
        alert("You don't have any money on the bank")
    }
    else if (loanGranted === false) {
        loanGranted = true
        work.takeAloan()
    }  else {
        let alreadyTakenALoan = document.getElementById("loan")
        alert("You already have a loan") 
    } 
}
// read in the values from the computers and shows them on the site 
function getComputerInformaion() {
    let computerName = document.getElementById("Selection").value;
    computerArray.forEach(computer=> {
        if(computerName==computer.name){
            document.getElementById("feature").innerText=computer.feature
            document.getElementById("featureOnComputer").innerText=computer.description
            document.getElementById("buy").innerHTML=computer.price +" kr"
            document.getElementById("name").src=computer.logo
            document.getElementById("picture").src=computer.img
            
        }
    })
}
// Will take the price from the site and will compare the price with the amount that the bank has 
function buyComputer() {
    let test = document.getElementById("buy").innerHTML
    let price = parseInt(test)

    if(bank.account < price) {
        let showError = document.getElementById("Error")
        alert("Not enough money")
    }
    else {
        alert("You have a new computer")
        loanGranted = false
        bank.buyComputer(price)

    }
}

