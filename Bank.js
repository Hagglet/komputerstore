
class Bank {
    constructor() {
        this.account = 0;        
    } 
    // add money to the account 
    addMoneyToBank(value) {
        let getBankBalance = document.getElementById("Balance")
        this.account += value 
        getBankBalance.innerText = "Balance: " + this.account
    }
    // if a loan is granted the amount will double 
    takeALoanFromBank() {
        let getBankBalance = document.getElementById("Balance")
        this.account = this.account * 2
        getBankBalance.innerText = "Balance: " + this.account
    }   
    // If the user buys a computer the price of it will be draw from a account 
    // the new balance will be showed on the site 
    buyComputer(value) {
        console.log(value)
        let getBankBalance = document.getElementById("Balance")
        this.account = this.account - value
        getBankBalance.innerText = "Balance: " + this.account
    }
}